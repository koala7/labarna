\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{lscape}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage[contents=]{background}
\usepackage{poker}
\setkeys{poker}{inline=symbol}
\usepackage{hyperref}
\usepackage[numberedsection,nopostdot,nogroupskip]{glossaries}
\makeglossaries
\usepackage{wrapfig}
\usepackage{placeins}


% define custom key 'counterImage'  for \newglossaryentry
\glsaddstoragekey
 {counterImage}% key/field name
 {}% default value if not explicitly set
 {\counterImage}% custom command to access the value if required

% Custom glossary entry style
% - Adds the word 'Pages' before the page numbers
% - Adds the counterImage
\newglossarystyle{listWithPagesAddition}{%
    \setglossarystyle{list}% base this style on the list style
    \renewcommand*{\glossentry}[2]{%
    \item[\glsentryitem{##1}%
        \glstarget{##1}{\glossentryname{##1}}]
        \glossentrydesc{##1}\glspostdescription\space \textemdash{}~Pages~##2\counterImage{##1}}%
}
\setglossarystyle{listWithPagesAddition}

\newglossaryentry{labarna}
{
    name=Labarna,
    description={Title of the kings of the Hittite empire. Named after Labarna I, the traditional founder of the realm. Usage similar to the title \textit{Cesar}.}
}

\newglossaryentry{assyrians}
{
    name=Assyrians,
    description={The Middle Assyrian Empire is your foe to the south east. Their heartland is in modern day Iraq and Syria, but they are aggressively moving towards your border.}
}

\newglossaryentry{seapeoples}
{
    name=Sea Peoples,
    description={Raiders and invaders from the west and south of unknown origin.}
}

\newglossaryentry{event}
{
    name=event,
    description={Random events are happening once per turn (section~\ref{sequence}). They are determined by your card deck. Consult the event-chart in the top left of the map print-out to determine which event follows the drawn card. Section~\ref{events} provides an overview on how to conduct all the events.}
}

\newglossaryentry{feastforthegods}
{
    name=Feast for the Gods,
    description={The Feast for the Gods happens once at the start of the game. There you influence the random distribution of the events a little. You have to distribute the four 7 cards between the events \textit{Famine}, \textit{Sea Peoples raid} and \textit{Assyrian aggression}. You can put all four to one event or balance them out more. Allocating cards to an event increases its likelihood of appearance. Section~\ref{feast} provides a detailed description.}
}

\newglossaryentry{worker}
{
    name=worker,
    description={Workers represent your population. They are either farmers (i.e. stationed in \hatti{}) or soldiers (i.e. stationed in \hapalla{}, \kizzuwatna{} or \nuhasse{}). You start the game with 10 and there is a total pool of 30.},
    counterImage={\\\includegraphics[width=2cm]{worker}}
}

\newglossaryentry{soldier}
{
    name=soldier,
    description={see \textit{worker}.}
}

\newglossaryentry{farmer}
{
    name=farmer,
    description={see \textit{worker}.}
}

\newglossaryentry{territories}
{
    name=territories,
    description={Your realm is divided into four territories, \hatti{} (with your capital ), \hapalla{}, \kizzuwatna{} and \nuhasse{}. They are marked on the map.}
}

\newglossaryentry{eventchart}
{
    name=event-chart,
    description={Chart printed in the top left of the map which helps you determine the event to a given card.}
}

\newglossaryentry{famineimminent}
{
    name=Famine imminent,
    description={Marker received from the \textit{Volcanic eruption} event. It indicates an extra \textit{Famine} event in the next turn.},
    counterImage={\\\includegraphics[width=2cm]{famineimminent}}
}

\newglossaryentry{assyrianoccupation}
{
    name=Assyrian occupation,
    description={Marker that indicates a territory is occupied by the Assyrians. Received from the \textit{Assyrian aggression} event. If the Sea Peoples attacked an occupied territory, the weakened version is used.},
    counterImage={\\\includegraphics[width=2cm]{assyrianoccupation} \includegraphics[width=2cm]{assyrianoccupationweakened}}
}

\usepackage{accanthis}

\newcommand{\rup}[1]{\left \lceil #1 \right \rceil}
\newcommand{\rdown}[1]{\left \lfloor #1 \right \rfloor}
\newcommand{\tip}[1]{\begin{quote}\hspace{-0.7cm}$\hookrightarrow$\hspace{0.3cm}#1\end{quote}}
\newcommand{\hatti}[0]{\textit{Hatti}}
\newcommand*{\nuhasse}[0]{\textit{Nuhašše}}
\newcommand{\hapalla}[0]{\textit{Hapalla}}
\newcommand{\kizzuwatna}[0]{\textit{Kizzuwatna}}
%\newcommand{\emptyCard}[0]{\framebox{\phantom{M}}}

\newcommand{\emptyCard}[0]{\fbox{\parbox[c][3.5mm]{6mm}{\phantom{}}}}

%\subject{\vspace{-2cm}Ludum Dare 49}
\title{\vspace{-2cm}\Huge Labarnas}
\subtitle{\vspace{0.2cm}The final struggle of the Hittite Empire\vspace{0.2cm}}
\author{A narrative board game}
\date{\setcounter{footnote}{1}\href{mailto:post@simon-lenz.de}{Simon Lenz} \\ \vspace{0.4cm}Version 1.7\footnote{The newest version can be always found at \url{https://www.simon-lenz.de/labarnas/labarnas.html}} — April 2023\vspace{-0.7cm}}

\begin{document}
\newgeometry{bottom=4.5cm, top=3.2cm}
\maketitle

\begin{center}
    \includegraphics[width=4cm]{hittite}\\
    \vspace{-0.2cm}
    \vspace{1cm}Players: Solo $\Big|$ Play while exploring the rules $\Big|$ Time: 20 minutes
\end{center}


\section{Introduction}
\textit{Labarnas} is a narrative game. Strategy is involved, but if the gods are not in your favour, there won't be a happy end. So lean back and let the story unfold.\\

The year is about 1200 B.C. You are Šuppiluliuma II., the last \Gls{labarna} (i.e. king) of the Hittite Empire. The glorious days of your ancestors are past. The extraordinary victory against the Egyptians at Kadesh has deteriorated to being merely a story. Your realm is crumbling.

Nature itself conspires against you. The climate worsened, and your people struggle with droughts and dire straits. They demand better living conditions and must be kept at bay. All the while, the vexatious \Gls{assyrians} long for the southern territories. Additionally, a new foe, the mysterious \Gls{seapeoples}, are raiding your western territories. And if those do not deal the mortal blow, there is always the looming thread of a disastrous catastrophe from the smoking mountain.

But do not despair. Your people are strong and have shaped the region for the better part of a millennium. With a wise leader like you, destiny \textit{can} be altered.\\

\clearpage
\restoregeometry
\section{Requirements}
To play \textit{Labarnas}, you'll need
\begin{itemize}
    \item a deck of cards (32 cards; french-suited\footnote{7, 8, 9, 10, J, Q, K and A, each in spades, hearts, diamonds, and clubs})
    \item one six sided die
    \item the printed map on page~\pageref{map} (DIN A4)
    \item the printed and cut out counter-sheet on page~\pageref{countersheet} (DIN A4)
\end{itemize}

You don't need to study the rules to play. Simply start your first game while progressing step by step through section~\ref{howtoplay}, and you'll explore the rules as you play.

\tip{When confused, a look into the Glossary section might help}

\section{How to play}\label{howtoplay}
\subsection{Synopsis}
You have to guide your people through a series of 30 random \glspl{event}. At the start of the game, you will influence the likelihood of certain \glspl{event} in the \textit{\Gls{feastforthegods}}. But your main task through the game will be the distribution of your population between food production and border defence.

\subsection{Preparation}\label{prep}
\subsubsection{Feast for the Gods}\label{feast}
The very first act of the game is the \textit{Feast for the Gods}. As the esteemed Labarna surely knows, the land of Hatti is the land of a thousand gods. Naturally we will start the game with a feast in their honour. The feast allows you to influence the destiny of your people, if only a little. Should the gods weaken your beastly enemies or protect your crops from withering to droughts? Whatever you decide, you have to make compromises.\\

You may distribute the four 7 cards (\sevc\sevs\sevh\sevd) freely between the events \textit{Famine}, \textit{Sea Peoples raid} and \textit{Assyrian aggression}. Events with more allocated cards will appear more often. To indicate your choice, place the four markers from the counter-sheet on your selected fields on the \gls{eventchart} on the map print-out.

\tip{If this is your first game, simply start with a balanced distribution of two (e.g. \sevc\sevs) allocated to \textit{Famine}, one (e.g. \sevh) allocated to the \textit{Sea Peoples raid} and the last (e.g. \sevd) allocated to the \textit{Assyrian aggression} event.}

\subsubsection{Initial worker distribution}
After the feast, you'll receive your initial 10 \glspl{worker}, which you need to distribute between your four territories on the map. \Glspl{worker} in the \hatti{} territory are \glspl{farmer} and produce food for you. Workers in the three border territories are \glspl{soldier} and will defend your realm from foreign enemies. The number of workers will increase as well as decrease through events. You will distribute your workers in every turn.

\tip{If this is your first game, you can start with a balanced distribution of 4 farmers in \hatti{}, 4 soldiers in \nuhasse{}, and one soldier each in \kizzuwatna{} and \hapalla{}.}

\subsubsection{Shuffle the event deck}
Now you need to shuffle your deck of cards. After shuffling, take — without looking — two random cards out of the deck and put them away. They will not be used during the game.

\subsection{Turn sequence}\label{sequence}
After the preparation is done, you will cycle through the following phases in every turn until one of the end conditions is met.

\begin{enumerate}
    \item \textbf{Reorganize:} Reorganize your \glspl{worker} as you please between all \gls{territories} you own. There must be at least one farmer in \hatti{}.\label{startofturn}
    \item \textbf{Draw:} Draw one card and determine the event type according to the \gls{eventchart}.
    \item \textbf{Conduct event:} Conduct \gls{event} according to section~\ref{events}.
    \item \textbf{Optional additional Famine:} If you have the \textit{\gls{famineimminent}} marker from the previous(!) turn, conduct now an extra \textit{Famine} event. Remove the marker afterwards.\label{endofturn}
\end{enumerate}

\subsection{End conditions}
\begin{description}
    \item[Victory] You've run through the whole turn sequence for all 30 \glspl{event} without triggering the Defeat conditions.
    \item[Defeat] There are two ways of losing the game.
        \begin{enumerate}
            \item You are not able to muster one \gls{farmer} in the \hatti{} territory during the \textit{reorganize} phase (section~\ref{sequence}).
            \item The \gls{assyrians} successfully take \hatti{} during the \textit{Assyrian aggression} event.
        \end{enumerate}
\end{description}

\newpage
\section{Events}\label{events}
\begin{description}
    \item[Prosperous year] Roll die and receive workers from the worker pool, equal to the number of pips or your number of farmers, whichever is less.
    \item[Famine] Roll die and lose \glspl{farmer} equal to the number of pips.
    \item[Sea Peoples raid] Conduct the following
        \begin{enumerate}
            \item Determine target: Roll die and compare the number of pips with the indicators on the map.
            \begin{enumerate}
                \item[] If the target territory is undefended, you lose all \glspl{farmer}. The event is finished.
                \item[] If the target is occupied by the \gls{assyrians}, replace the \textit{\gls{assyrianoccupation}} marker with its weakened version. The event is finished.
                \item[] Else, continue the attack.
            \end{enumerate}
            \item Attack: Roll die. The defence was successful if you have more defenders than the number of pips. A roll of a $6$ is the exception and will always result in your defeat.
            \begin{enumerate}
                \item[Success] Nothing happens.
                \item[Failure] You lose one defender in the raided territory, and half of your farmers rounded down, but at least one.
            \end{enumerate}
        \end{enumerate}
    \item[Assyrian aggression]Conduct the following
        \begin{enumerate}
            \item Determine target: Next adjacent territory along red Assyrian arrows on the map (\nuhasse{} $\rightarrow$ \kizzuwatna{} $\rightarrow$ \hatti{}).
            \item Attack: Roll die. If the attacking Assyrians have the \textit{\gls{assyrianoccupation} (weakened)} marker, subtract $1$ from the die roll. The defence was successful if you have more defenders than the number of pips. A roll of a $6$ is the exception and will always result in your defeat. Note that \hatti{} has a fixed number of 3 defenders.
                \begin{enumerate}
                    \item[Success] Nothing happens.
                    \item[Failure] Lose all defenders. From now on, the Assyrians occupy the territory. To indicate that, put the \textit{\gls{assyrianoccupation}} marker on the territory. Future \textit{Assyrian aggressions} will start from the occupied territory. If the occupied territory is \hatti{}, the game is lost immediately.
                    \item[in any case] If the Assyrians had the weakened \textit{\gls{assyrianoccupation}} marker, replace it with the normal one.
                \end{enumerate}
        \end{enumerate}
    \newpage
    \item[Civil uprising] Roll die. The counterinsurgency was successful if the number of pips is less than half of your farmers rounded up. A roll of a $6$ is the exception and will always result in your defeat.
            \begin{enumerate}
                \item[Success] Nothing happens.
                \item[Failure] Lose half of your \glspl{farmer} rounded down, but at least one.
            \end{enumerate}
    \item[Volcanic eruption] Lose immediately half of your \glspl{farmer} rounded down, but at least one. Also put the \gls{famineimminent} marker in the \hatti{} territory. It will play a role in the \textit{additional Famine} phase of your next turn (section~\ref{sequence}).
\end{description}

\printglossary

\section{Sources \& Inspiration}
While this game is not an actual representation of anything, it drew inspiration from how, we think, things might have happened.

\begin{itemize}
    \item Karen Radner \& Eleanor Robson — The Oxford Handbook of Cuneiform Culture; 2011 Oxfod University Press
    \item C.W. Ceram — Enge Schlucht und schwarzer Berg, Entdeckung des Hethiter Reichs; 1955 Rowohlt (Contains inspiration for the map)
    \item Eric H. Cline — 1177 B.C.: The Year Civilization Collapsed; 2015
    \item Picture: \href{https://commons.wikimedia.org/wiki/File:Fragment_of_a_stele_showing_a_woman_and_a_horse_rider._Probably_from_Kahramanmara%C5%9F,_Turkey.jpg}{Fragment of a stele showing a woman and a horse rider. Probably from Kahramanmaras, Turkey}; Osama Shukir Muhammed Amin FRCP(Glasg), CC BY-SA 4.0 via Wikimedia Commons
\end{itemize}

\vspace{0.5cm}
Feedback of all kinds is very welcome at \href{mailto:post@simon-lenz.de}{post@simon-lenz.de}.

\vspace{1.5cm}
\Huge\hfill Have Fun!

\newpage
\thispagestyle{empty}
\newgeometry{top=10mm, bottom=10mm, left=10mm, right=10mm}

\begin{landscape}\label{map}

\backgroundsetup{
    scale=1,
    angle=90,
    opacity=1,
    contents={\includegraphics[width=\paperheight-1cm,height=\paperwidth-1cm]{map}}
}


\hfill
\vspace{-1.05cm}
\FloatBarrier
\begin{table}[h]
\def\arraystretch{1.5}
\begin{tabular}{ll}
Event & Associated cards  \\
\hline
Prosperous year & \eigc \eigs \eigh \eigd \ninec \nines \nineh \nined \tenc \tens \tenh \tend \\
Famine & \Jc \Js \Jh \Jd \emptyCard{} \emptyCard{} \emptyCard{} \emptyCard{} \\
Sea Peoples raid & \Qc \Qs \Qh \Qd \emptyCard{} \emptyCard{} \emptyCard{} \emptyCard{} \\
Assyrian aggression & \Ac \As \Ah \emptyCard{} \emptyCard{} \emptyCard{} \emptyCard{} \\
Civil uprising & \Kh \Kd \Ad \\
Volcanic eruption & \Kc \Ks \\
\end{tabular}
\end{table}
\FloatBarrier

\end{landscape}

\restoregeometry


\thispagestyle{empty}
\newgeometry{top=20mm, bottom=10mm, left=10mm, right=10mm}

\begin{center}\label{countersheet}
\includegraphics[width=17cm]{counters}
    
\end{center}

\restoregeometry

\end{document}
